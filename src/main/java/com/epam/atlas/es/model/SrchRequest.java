package com.epam.atlas.es.model;


/**
 * Created by Marko on 20.09.2017.
 */
public class SrchRequest {
    private String originalQuery;
    private User user;

    public String getOriginalQuery() {
        return originalQuery;
    }

    public void setOriginalQuery(String originalQuery) {
        this.originalQuery = originalQuery;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}