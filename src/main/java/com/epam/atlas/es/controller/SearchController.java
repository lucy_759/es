package com.epam.atlas.es.controller;

import com.epam.atlas.es.model.SrchRequest;
import com.epam.atlas.es.model.User;
import com.epam.atlas.es.service.ExpertSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * Created by Marko on 20.09.2017.
 */
@RestController
public class SearchController {
    @Autowired
    private ExpertSystemService expertSystemService;

    @RequestMapping(path = "es/search", method = RequestMethod.POST)
    public Set<String> findTags(@RequestBody User user, @RequestParam(name = "q") String query) {
        SrchRequest srchRequest = new SrchRequest();
        srchRequest.setOriginalQuery(query);
        srchRequest.setUser(user);
        return expertSystemService.findTags(srchRequest).getTags();
    }
}
