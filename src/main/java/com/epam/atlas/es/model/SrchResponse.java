package com.epam.atlas.es.model;

import java.util.HashSet;
import java.util.Set;

public class SrchResponse {
    private Set<String> tags = new HashSet<>();

    public Set<String> getTags() {
        return tags;
    }

    public Boolean addTag(String tag) {
        return tags.add(tag);
    }

}
