package com.epam.atlas.es.service;

import com.epam.atlas.es.model.SrchRequest;
import com.epam.atlas.es.model.SrchResponse;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Marko on 20.09.2017.
 */
@Service
public class ExpertSystemService {

    private static final String GLOBAL_TAGS = "srchResponse";

    @Autowired
    private KieContainer kieContainer;

    public SrchResponse findTags(SrchRequest srchRequest) {
        KieSession kieSession = kieContainer.newKieSession();
        SrchResponse srchResponse = new SrchResponse();
        kieSession.setGlobal(GLOBAL_TAGS, srchResponse);
        kieSession.insert(srchRequest);
        kieSession.insert(srchResponse);
        kieSession.fireAllRules();
        kieSession.dispose();
        return srchResponse;
    }
}
